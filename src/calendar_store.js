import { observable, action } from 'mobx'

class CalendarStore {
  constructor() {
    this.buildMonthTable()
  }

  @observable version = 1
  @observable monthTable = [[], [], [], [], [], []]
  @observable selectedDate = new Date()
  @observable selectedMonth = new Date(new Date().setDate(1))
  @observable selectedDay = null
  @observable selectedEvent = 0
  @observable dates = []

  @action.bound
  setDates(array) {
    this.dates = array
  }

  @action.bound
  setVersion(version) {
    this.version = version
  }

  @action
  buildMonthTable() {
    this.monthTable = [[], [], [], [], [], []]

    const d = new Date(this.selectedMonth)
    const daysOfSelectedMonth =
      new Date(d.getFullYear(), d.getMonth() + 1, 0).getDate()
    const daysOfPastMonth =
      new Date(d.getFullYear(), d.getMonth(), 0).getDate()
    let firstDayOfMonth = d.getDay() - 1 === -1 ? 6 : d.getDay() - 1 || 7

    let count = 1
    let newMonthCount = 1

    this.monthTable.forEach((row, i) => {
      if (i === 0) {
        for (let j = firstDayOfMonth; j > 0; j -= 1) {
          row.push(
            new Date(d).setMonth(d.getMonth() - 1, daysOfPastMonth - j + 1)
          )
        }

        while (row.length < 7) {
          row.push(new Date(d).setDate(count))
          count += 1
        }
      } else {
        for (let j = 0; j < 7; j += 1) {
          if (count <= daysOfSelectedMonth) {
            row.push(new Date(d).setDate(count))
            count += 1
          } else {
            row.push(new Date(d).setMonth(d.getMonth() + 1, newMonthCount))
            newMonthCount += 1
          }
        }
      }
    })
  }

  @action
  setSelectedDate(date) {
    this.selectedDate = date
    this.selectedMonth = date
  }

  @action
  setSelectedDay(day) {
    this.selectedDay = day
  }

  @action
  setSelectedEvent(event) {
    this.selectedEvent = event
  }

  @action
  nextMonth() {
    const d = new Date(new Date(this.selectedMonth).setDate(1))
    this.selectedMonth = new Date(d.setMonth(d.getMonth() + 1))
    this.buildMonthTable()
  }

  @action
  prevMonth() {
    const d = new Date(new Date(this.selectedMonth).setDate(1))
    this.selectedMonth = new Date(d.setMonth(d.getMonth() - 1))
    this.buildMonthTable()
  }

  @action
  nextEvent() {
    if (this.selectedDay && this.selectedDay.events) {
      if (this.selectedEvent + 1 <= this.selectedDay.events.length - 1) {
        this.selectedEvent += 1
      } else {
        this.selectedEvent = 0
      }
    }
  }

  @action
  prevEvent() {
    if (this.selectedDay && this.selectedDay.events) {
      if (this.selectedEvent > 0) {
        this.selectedEvent -= 1
      } else {
        this.selectedEvent = this.selectedDay.events.length - 1
      }
    }
  }
}

export default new CalendarStore()
