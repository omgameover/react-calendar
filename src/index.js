import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'mobx-react'
import './index.sass'
import App from './App'
import registerServiceWorker from './registerServiceWorker'
import calendarStore from './calendar_store'

const store = {
  calendarStore
}

ReactDOM.render(
  <Provider {...store}><App /></Provider>,
  document.getElementById('react-calendar')
)
registerServiceWorker()
