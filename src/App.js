import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { inject, observer } from 'mobx-react'
import Moment from 'react-moment'
import lodash from 'lodash'
import 'moment/locale/ru'
import './index.sass'

@inject('calendarStore')
@observer
class App extends Component {
  constructor(props) {
    super(props)
    this.store = this.props.calendarStore
  }

  componentDidMount() {
    const dates = ReactDOM.findDOMNode(this).parentNode.getAttribute('dates')
    const version = ReactDOM.findDOMNode(this).parentNode.getAttribute('version')
    if (dates && JSON.parse(dates))
      this.store.setDates(this.convertDatesToRightFormat(JSON.parse(dates)))
    if (version) this.store.setVersion(parseInt(version, 10))
    this.store.setSelectedDay(this.findDay(new Date()))
  }

  convertDatesToRightFormat(dates) {
    const rightFormat = []
    let tempDates = {}

    dates.forEach(day => {
      if (!(day.date in tempDates)) {
        tempDates[day.date] = {}
        tempDates[day.date].date = day.date
        tempDates[day.date].events = []
      }

      tempDates[day.date].events.push({
        time: day.time,
        address: day.address,
        title: day.title,
        description: day.description
      })
    })

    Object.values(tempDates).forEach(date => {
      rightFormat.push(date)
    })

    return rightFormat
  }

  _renderRows() {
    return this.store.monthTable.map((row, i) => {
      return (
        <div className='react-calendar-number-row' key={i}>
          {this._renderNumbers(row)}
        </div>
      )
    })
  }

  _renderNumbers(row) {
    return row.map((number, i) => {
      const checkSelectedMonth = () => {
        return (
          new Date(number).getMonth() === this.store.selectedMonth.getMonth()
        )
      }

      const checkSelectedDate = () => {
        return (
          new Date(number).getDate() === this.store.selectedDate.getDate() &&
          new Date(number).getMonth() === this.store.selectedDate.getMonth()
        )
      }

      let className = `react-calendar-number-${this.store.version}`

      if (!checkSelectedMonth()) className += ' react-calendar-not-present'
      if (checkSelectedDate() && checkSelectedMonth())
        className += ' react-calendar-selected'

      return (
        <div
          className={className}
          key={i}
          onClick={() => this.onDate(new Date(number))}
        >
          {this._renderEventMark(number)}
          {new Date(number).getDate()}
        </div>
      )
    })
  }

  onDate(date) {
    if (date.getMonth() === this.store.selectedMonth.getMonth() - 1) {
      this.store.prevMonth()
    } else if (date.getMonth() === this.store.selectedMonth.getMonth() + 1) {
      this.store.nextMonth()
    }

    this.store.setSelectedDay(this.findDay(date))
    this.store.setSelectedDate(date)
  }

  findDay(date) {
    const mm = date.getMonth() + 1 > 9 ?
      `${date.getMonth() + 1}` : `0${date.getMonth() + 1}`
    const dd = date.getDate() > 9 ?
      `${date.getDate()}` : `0${date.getDate()}`

    const stringDate =
      `${date.getFullYear()}.${mm}.${dd}`

    return lodash.find(this.store.dates, d => {
      return d.date === stringDate
    })
  }

  _renderEventMark(number) {
    const day = this.findDay(new Date(number))
    if (day && day.events.length !== 0) return (
      <div className={`react-calendar-item-mark-${this.store.version}`}></div>
    )
  }

  _renderDaysNames() {
    const daysNames = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс']

    return daysNames.map((dayName, i) => {
      return (
        <div
          className={`react-calendar-day-name-${this.store.version}`}
          key={i}
        >
          {dayName}
        </div>
      )
    })
  }

  render() {
    const time =
      this.store.selectedDay ?
      this.store.selectedDay.events[this.store.selectedEvent].time.split(':') :
      null

    const date = this.store.selectedDay ?
      this.store.selectedDay.date.split('.') : null

    return (
      <div className='react-calendar-calendar-container'>
        <div className='react-calendar-calendar-block'>
          <div className={`react-calendar-calendar version-${this.store.version}`}>
            <div className='react-calendar-header-block'>
              <div className='react-calendar-header'>
                <button
                  className={`react-calendar-button-${this.store.version} react-calendar-left`}
                  onClick={() => this.store.prevMonth()}>&#60;</button>
                <div className='react-calendar-date-container'>
                  <div className='react-calendar-date'>
                    <div className='react-calendar-month'>
                      <Moment format='MMMM' date={this.store.selectedMonth} />
                    </div>
                    <div className={`react-calendar-year-${this.store.version}`}>
                      <Moment format='YYYY' date={this.store.selectedMonth} />
                    </div>
                  </div>
                </div>
                <button
                  className={`react-calendar-button-${this.store.version} react-calendar-right`}
                  onClick={() => this.store.nextMonth()}>&#62;</button>
              </div>
            </div>
            <div className='react-calendar-days-names-block'>
              <div className='react-calendar-days-names-container'>
                <div className='react-calendar-days-names'>{this._renderDaysNames()}</div>
              </div>
            </div>
            <div className='react-calendar-numbers-block'>
              <div className='react-calendar-numbers-container'>
                {this._renderRows()}
              </div>
            </div>
          </div>
          {this.store.version !== 3 && <div className='react-calendar-hint'>
            В календаре отмечены дни, на которые планируется мероприятия.
            Нажмите на число, чтобы узнать подробности
          </div>}
        </div>

        <div className={`react-calendar-about-event version-${this.store.version}`}>
          {!this.store.selectedDay &&
            <div className='react-calendar-no-events'>
              Событий нет
            </div>
          }
          {this.store.selectedDay && <div>
            {this.store.version === 3 && <div className='selected-event'>
              {this.store.selectedEvent + 1} событие
            </div>}
            <div className={`react-calendar-header version-${this.store.version}`}>
              {this.store.selectedDay.events.length === 1 && <span>
                {this.store.selectedDay.events.length} событие
              </span>}
              {this.store.selectedDay.events.length >= 2 &&
              this.store.selectedDay.events.length <= 4 && <span>
                {this.store.selectedDay.events.length} события
              </span>}
              {this.store.selectedDay.events.length > 4 && <span>
                {this.store.selectedDay.events.length} событий
              </span>}
            </div>
            {this.store.version !== 3 && <div className='react-calendar-address-time-container'>
              <div className='react-calendar-address-time'>
                <div className={`react-calendar-address-${this.store.version}`}>
                  <div className={`react-calendar-icon react-calendar-pin-${this.store.version}`} />
                  <span className='text'>
                    {this.store.selectedDay.events[this.store.selectedEvent].address}
                  </span>
                </div>
                <div className={`react-calendar-time-${this.store.version}`}>
                  <div className={`react-calendar-icon react-calendar-calendar-${this.store.version}`} />
                  <span className='text'>
                    <Moment
                      format='D MMMM YYYY'
                      date={{ year: date[0], month: date[1] - 1, day: date[2] }}
                    />
                    &nbsp;&nbsp;
                    <Moment
                      format='HH:mm'
                      date={{ hour: time[0], minute: time[1] }}
                    />
                  </span>
                </div>
              </div>
            </div>}
            <div className='react-calendar-event-info'>
              <div className='react-calendar-title-container'>
                <div className='react-calendar-title'>
                  {this.store.selectedDay.events[this.store.selectedEvent].title}
                  {this.store.version === 1 &&
                    <div className='react-calendar-line'></div>
                  }
                </div>
              </div>
              <div className='react-calendar-description-container'>
                <div
                  className='react-calendar-description'
                  dangerouslySetInnerHTML={{
                    __html: this.store.selectedDay
                      .events[this.store.selectedEvent].description
                  }}
                ></div>
              </div>
            </div>
            {this.store.version === 3 && <div className='react-calendar-address-time-container'>
              <div className='react-calendar-address-time'>
                <div className={`react-calendar-address-${this.store.version}`}>
                  <div className={`react-calendar-icon react-calendar-pin-${this.store.version}`} />
                  <span className='text'>
                    {this.store.selectedDay.events[this.store.selectedEvent].address}
                  </span>
                </div>
                <div className={`react-calendar-time-${this.store.version}`}>
                  <div className={`react-calendar-icon react-calendar-calendar-${this.store.version}`} />
                  <span className='text'>
                    <Moment
                      format='D MMMM YYYY'
                      date={{ year: date[0], month: date[1] - 1, day: date[2] }}
                    />
                    &nbsp;&nbsp;
                    <Moment
                      format='HH:mm'
                      date={{ hour: time[0], minute: time[1] }}
                    />
                  </span>
                </div>
              </div>
            </div>}
            {this.store.selectedDay.events.length > 1 &&
              <div className={`react-calendar-buttons version-${this.store.version}`}>
                <button
                  className={`react-calendar-button-${this.store.version} react-calendar-left`}
                  onClick={() => this.store.prevEvent()}
                >
                  <span className='arrow'>&#60;</span>
                  <span className='arrow-text'> предыдущее</span>
                </button>
                <button
                  className={`react-calendar-button-${this.store.version} react-calendar-right`}
                  onClick={() => this.store.nextEvent()}
                >
                  <span className='arrow-text'>следующее </span>
                  <span className='arrow'>&#62;</span>
                </button>
              </div>
            }
          </div>}
        </div>
      </div>
    )
  }
}

export default App
