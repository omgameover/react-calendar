### How to use
```html
<div
  id="react-calendar"
  dates="[DATA IN JSON FORMAT]"
  version="[VERSION NUMBER 1-3]"
/>
<script type="text/javascript" src="[PATH TO JS FILE]"></script>

```

### Data structure
```javascript
[
  {
    date: '2018.02.10',
    time: '15:45',
    address: '',
    title: '',
    description: ''
  },
  ...
]
```

### Versions

#### 1
![calendar 1 example](calendar1.png)

#### 2
![calendar 2 example](calendar2.png)

#### 3
![calendar 3 example](calendar3.png)
